# Overview

A VueJS application that takes a JSON blob from githubusercontent.com and displays a list of properties.

# Demo

A demo can be seen here: http://hw-tech-test.s3-website-eu-west-1.amazonaws.com/

# To run locally

`npm i`
`npm run dev`
Navigate to `http://localhost:8080/` locally.

# Notes

1. The infinite scroll is basic but suits what's needed for this type of demo, normally it'd need a loading experience as it grabs more paginated data from the API.
2. There's no onClick events for the buttons etc, this seemed beyond the scope of the demo.
3. I didn't try to slim down the ~400KB bundle size, but I imagine that wouldn't be too hard, if it was hosted somewhere that supported gziping it'd be reduced significantly anyway.
4. Vuex might have been overkill, but I think VueX is a good choice for pretty much any Vue app and if this were to undergo further work it would be the right architectural decision.
