export default {
  getProperties() {
    return fetch(
      `https://gist.githubusercontent.com/ruimendesM/bf8d095f2e92da94938810b8a8187c21/raw/70b112f88e803bf0f101f2c823a186f3d076d9e6/properties.json`,
      { method: "GET" }
    )
      .then(response => response.text())
      .then(response => {
        return JSON.parse(response);
      });
  }
};
