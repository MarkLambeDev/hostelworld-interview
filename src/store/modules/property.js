import property from "../../api/property";
import { convertToEuro } from "../../utils/utils.js";

// initial state
const state = {
  properties: [],
  location: {},
  highestPricePerNight: {},
  lowestPricePerNight: {},
  propertyCount: 0
};

// getters
const getters = {};

// actions
const actions = {
  getAllProperties({ commit }) {
    property.getProperties().then(propertyData => {
      commit("setProperties", propertyData);
    });
  }
};

// mutations
const mutations = {
  setProperties(state, propertyData) {
    let properties = propertyData.properties.map(property => {
      let images = property.images.map(
        img => `http://${img.prefix}${img.suffix}`
      );

      let lowestPrivatePricePerNight = null;
      if (property.lowestPrivatePricePerNight) {
        lowestPrivatePricePerNight = convertToEuro(
          property.lowestPrivatePricePerNight.value,
          property.lowestPrivatePricePerNight.currency
        );
      }

      let lowestDormPricePerNight = null;
      if (property.lowestDormPricePerNight) {
        lowestDormPricePerNight = convertToEuro(
          property.lowestDormPricePerNight.value,
          property.lowestDormPricePerNight.currency
        );
      }

      let freeWifi = property.facilities
        .find(fac => fac.id === "FACILITYCATEGORYFREE")
        ?.facilities?.some(fac => fac.id === "FREEWIFI");

      let breakfastIncluded = property.facilities
        .find(fac => fac.id === "FACILITYCATEGORYFREE")
        ?.facilities?.some(fac => fac.id === "BREAKFASTINCLUDED");

      let distanceToCCDesc = property.distance
        ? `${property.distance.value} ${property.distance.units} from city center`
        : null;

      return {
        id: property.id,
        type: property.type,
        name: property.name,
        overview: property.overview,
        overallRating: property.overallRating,
        images,
        lowestPrivatePricePerNight,
        lowestDormPricePerNight,
        freeWifi,
        breakfastIncluded,
        distanceToCCDesc,
        overallRating: property.overallRating,
        isFeatured: property.isFeatured
      };
    });

    state.properties = properties;
    state.location = propertyData.location;
    state.highestPricePerNight = convertToEuro(
      propertyData.filterData.highestPricePerNight.value,
      propertyData.filterData.highestPricePerNight.currency
    );
    state.lowestPricePerNight = convertToEuro(
      propertyData.filterData.lowestPricePerNight.value,
      propertyData.filterData.lowestPricePerNight.currency
    );
    state.propertyCount = properties.length;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
