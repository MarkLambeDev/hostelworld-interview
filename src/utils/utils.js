export function convertToEuro(value, currency = "VEF") {
  if (currency === "VEF") {
    return Math.round((value / 7.55) * 100) / 100;
  } else if (currency === "EUR") {
    return Math.round(value * 100) / 100;
  }
}
